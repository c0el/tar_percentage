# tar_percentage

This shell script will show you the process of archiving/unarchiving of files, and/or folders in the forms of percentage.

Here's an example of how it will look like:<br />
![percentage](test.png)

⚠️⚠️⚠️Archiving works fine, but unarchiving is currently in development.⚠️⚠️⚠️