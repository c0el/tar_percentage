#!/bin/bash

# Compress and decompress archives
archive="$1"
 
originalsize=$(file $archive | rev | cut -d' ' -f1 | rev)
step=100
blocks=$(echo "$originalsize / 512 / 20 / $step" | bc)

PS3="Choose an option or press ctrl+z to quit: "
options=("Create an archive" "Decompress an archive")
select opt in "${options[@]}"
do
	case $opt in
		"Create an archive")
			tar cf - $archive -P | pv -s $(du -sb $archive | awk '{print $1}') | gzip > "$archive".tar.gz
			;;
		"Decompress an archive")
			tar cf - $archive -P | pv -s $(du -sb $archive | awk '{print $1}') | gzip > "$archive".tar.gz
esac
done
